<?php

/**
 * Class Ah_Debug_IndexController
 * @author Aydin Hassan <aydin@wearejh.com>
 */
class Ah_Debug_IndexController extends Mage_Core_Controller_Front_Action
{

    /**
     * Route accessible at /debug/index/index or just /debug
     */
    public function indexAction()
    {

    }
} 