# Ah Magento Debug

This module provides some debigging tools to help with my development. Created a module so I can easily drop it into new projects. At the moment it just provides a controller to allow
me to run various pieces of code easily with the Magento application.

The route is available at /debug. Just drop in some code to the provided and controller and run!

## Installation

This module is installable via `Composer`. If you have used the Magento Skeleton as a base module then you can just require this project and all the rest is done for you. 

```
$ cd project-root
$ ./composer.phar require "aydinhassan/ah_magento_debug"
```

Note: As these repositories are currently private and not available via a public package list like [Packagist](https://packagist.org/) Or [Firegento](http://packages.firegento.com") you need to add the repository to the projects `composer.json` before you require the project.

```
"repositories": [
    {
        "type": "vcs",
        "url": "git@bitbucket.org:AydinHassan/ah_magento_debug.git"
    }
],
```

If you have not used the Magento Skeleton Application as a base you will need to install `Composer` and depending on whether it is a Magento project or standard PHP project some different steps are required. I'm going to omit these details as this is a Magento extension and you should probably be using the JH Magento Skeleton Application


## Generating Docs

Move to the cloned repository location, usually in the `vendor` directory of your Magento project.
Install composer dependencies and then run phpDocumentor2.

```
$ cd vendor/aydinhassan/ah_magento_debug
$ php composer.phar install --dev
$ vendor/bin/phpdoc.php -d src -t docs
```

The docs will be available in the ./docs directory!

## Running tests

Coming soon...